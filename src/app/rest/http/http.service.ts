import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {DataType, HouseType, HousesType, TokenType, UsersType, UserWithIdType} from '../../types';

@Injectable()
export class HttpService {
  constructor (private http: HttpClient) { }

  postLogin (data: DataType): Observable<TokenType> {
    return this.http.post<TokenType>('http://localhost:3000/admin', data);
  }

  pushNewUser (data: UsersType): Observable<TokenType> {
    return this.http.post<TokenType>('http://localhost:3000/create', data);
  }

  pushNewHouse (data: HouseType): Observable<HouseType> {
    return this.http.post<HouseType>('http://localhost:3000/admin/create', data);
  }

  getUserHouses (id: number): Observable<HousesType[]> {
    return this.http.get<HousesType[]>(`http://localhost:3000/admin/get_list/${id}`);
  }

  getAllHouses (): Observable<HousesType[]> {
    return this.http.get<HousesType[]>('http://localhost:3000/get_list');
  }

  getHouseInfo (houseId: number): Observable<HousesType> {
    return this.http.get<HousesType>(`http://localhost:3000/house_info/${houseId}`);
  }

  putHouseInfo (house: HousesType): Observable<any> {
    return this.http.put<HousesType>(`http://localhost:3000/house_info/${house.id}`, house);
  }

  deleteHouse (houseId: number): Observable<HousesType> {
    return this.http.delete<HousesType>(`http://localhost:3000/house_info/delete/${houseId}`);
  }

  getUserInfo (userId: number): Observable<UserWithIdType> {
    return this.http.get<UserWithIdType>(`http://localhost:3000/get_info/${userId}`);
  }
}
