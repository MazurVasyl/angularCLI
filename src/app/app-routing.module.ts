import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginGuard } from './modules/auth/guards/login.guard';
import { LoggedGuard } from './modules/auth/guards/logged.guard';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'houses-to-sale'
  },
  {
    path: 'houses-to-sale',
    loadChildren: () => import('./containers/houses-to-sale/houses-to-sale.module').then(m => m.HousesToSaleModule)
  },
  {
    path: 'registration',
    canActivate: [LoggedGuard],
    loadChildren: () => import('./containers/registration/registration.module').then(m => m.RegistrationModule)
  },
  {
    path: 'login',
    canActivate: [LoggedGuard],
    loadChildren: () => import('./containers/login/login.module').then(m => m.LoginModule)
  },
  {
    path: 'own-page/:id',
    canActivate: [LoginGuard],
    loadChildren: () => import('./containers/own-page/own-page.module').then(m => m.OwnPageModule)
  },
  {
    path: 'own-page/:id/create',
    canActivate: [LoginGuard],
    loadChildren: () => import('./containers/create-house/create-house.module').then(m => m.CreateHouseModule)
  },
  {
    path: 'detailed/:houseId',
    loadChildren: () => import('./containers/detailed-info/detailed-info.module').then(m => m.DetailedInfoModule)
  },
  {
    path: 'own-page/:id/edit/:houseId',
    canActivate: [LoginGuard],
    loadChildren: () => import('./containers/edit-house/edit-house.module').then(m => m.EditHouseModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
