import { NgModule } from '@angular/core';
import { LoginGuard } from './guards/login.guard';
import { LoggedGuard } from './guards/logged.guard';
import { LoginService } from './services/login.service';

@NgModule({
  providers: [
    LoginGuard,
    LoggedGuard,
    LoginService
  ]
})
export class AuthModule { }
