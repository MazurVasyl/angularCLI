import { Injectable } from '@angular/core';
import { UserWithIdType } from '../../../types';

@Injectable()
export class LoginService {
  private token = 'token';
  private id = 'id';
  private firstName = 'firstName';
  private lastName = 'lastName';
  private phone = 'phone';
  private email = 'email';

  public createToken (token: string, user: UserWithIdType): void {
    localStorage.setItem(this.token, token);
    localStorage.setItem(this.id, user.id.toString());
    localStorage.setItem(this.firstName, user.firstName);
    localStorage.setItem(this.lastName, user.lastName);
    localStorage.setItem(this.phone, user.phone);
    localStorage.setItem(this.email, user.email);
  }

  public getToken (): string {
    return localStorage.getItem(this.token);
  }

  public getId (): string {
    return localStorage.getItem(this.id);
  }

  public getFirstName (): string {
    return localStorage.getItem(this.firstName);
  }

  public getLastName (): string {
    return localStorage.getItem(this.lastName);
  }

  public destroyToken (): void {
    localStorage.removeItem(this.token);
    localStorage.removeItem(this.id);
    localStorage.removeItem(this.firstName);
    localStorage.removeItem(this.lastName);
    localStorage.removeItem(this.phone);
    localStorage.removeItem(this.email);
  }

  public isAuthorized (): boolean {
    if (localStorage.getItem(this.token)) {
      return true;
    } else {
      return false;
    }
  }
}
