import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Injectable()
export class LoginGuard implements CanActivate {
  constructor (
    private router: Router,
    private token: LoginService
  ) {}

  canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (!this.token.isAuthorized()) {
      this.router.navigateByUrl('/login');
      return false;
    }
    return true;
  }
}
