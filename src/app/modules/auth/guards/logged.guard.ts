import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoginService } from '../services/login.service';

@Injectable()
export class LoggedGuard implements CanActivate {
  constructor (
    private router: Router,
    private token: LoginService
  ) {}

  canActivate (next: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    console.log(this.token.isAuthorized());
    if (this.token.getToken()) {
      this.router.navigateByUrl('/');
      return false;
    }
    return true;
  }
}
