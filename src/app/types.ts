export type HouseType = {
  city: string;
  street: string;
  houseNumber: number;
  price: number;
  roomCount: number;
  userId: number;
};

export type UsersType = {
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
  login: string;
  password: string;
};

export type UserWithIdType = {
  id: number;
  firstName: string;
  lastName: string;
  phone: string;
  email: string;
};

export type HousesType = {
  id: number;
  city: string;
  street: string;
  houseNumber: number;
  price: number;
  roomCount: number;
  userId: number;
};

export type TokenType = {
  token: string;
  userId: number;
}

export type DataType = {
  login: string | undefined;
  password: string | undefined;
}
