import { Component, Input } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-range-length-paragraph',
  templateUrl: './range-length-paragraph.component.html'
})
export class RangeLengthParagraphComponent {
  @Input() field: NgModel;
  @Input() MINLength: number;
  @Input() MAXLength: number;
}
