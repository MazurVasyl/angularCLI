import { Component, Input } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-letters-paragraph',
  templateUrl: './letters-paragraph.component.html'
})
export class LettersParagraphComponent {
  @Input() field: NgModel;
}
