import { Component, Input } from '@angular/core';
import { NgModel } from '@angular/forms';

@Component({
  selector: 'app-pattern-error-paragraph',
  templateUrl: './pattern-error-paragraph.component.html'
})
export class PatternErrorParagraphComponent {
  @Input() field: NgModel;
}
