import { Component, Input } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-must-be-filled-paragraph',
  templateUrl: './must-be-filled-paragraph.component.html'
})
export class MustBeFilledParagraphComponent {
  @Input() form: NgForm;
  @Input() field: NgModel;
}
