import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MustBeFilledParagraphComponent } from './must-be-filled-paragraph/must-be-filled-paragraph.component';
import { RangeLengthParagraphComponent } from './range-length-paragraph/range-length-paragraph.component';
import { PatternErrorParagraphComponent } from './pattern-error-paragraph/pattern-error-paragraph.component';
import { LettersParagraphComponent } from './letters-paragraph/letters-paragraph.component';
import { MinErrorParagraphComponent } from './min-error-paragraph/min-error-paragraph.component';

@NgModule({
  declarations: [
    MustBeFilledParagraphComponent,
    RangeLengthParagraphComponent,
    PatternErrorParagraphComponent,
    LettersParagraphComponent,
    MinErrorParagraphComponent
  ],
  exports: [
    MustBeFilledParagraphComponent,
    RangeLengthParagraphComponent,
    PatternErrorParagraphComponent,
    LettersParagraphComponent,
    MinErrorParagraphComponent
  ],
  imports: [CommonModule]
})
export class ParagraphsModule { }
