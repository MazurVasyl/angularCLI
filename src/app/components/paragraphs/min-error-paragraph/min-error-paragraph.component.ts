import { Component, Input } from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';

@Component({
  selector: 'app-min-error-paragraph',
  templateUrl: './min-error-paragraph.component.html'
})
export class MinErrorParagraphComponent {
  @Input() form: NgForm;
  @Input() field: NgModel;
  @Input() minNumber: number;
}
