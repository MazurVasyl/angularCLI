import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../rest/http/http.service';
import { LoginService } from '../../../modules/auth/services/login.service';

@Component({
  selector: 'app-name',
  templateUrl: './name.component.html'
})
export class NameComponent implements OnInit {
  firstName: string;
  lastName: string;
  token: string;

  constructor (
    private http: HttpService,
    private loginService: LoginService
  ) {}

  ngOnInit (): void {
    this.token = this.loginService.getToken();
    this.firstName = this.loginService.getFirstName();
    this.lastName = this.loginService.getLastName();
  }
}
