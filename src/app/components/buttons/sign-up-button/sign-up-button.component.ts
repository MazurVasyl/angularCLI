import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../modules/auth/services/login.service';

@Component({
  selector: 'app-sign-up-button',
  templateUrl: './sign-up-button.component.html'
})
export class SignUpButtonComponent implements OnInit {
  token: string;

  constructor (private loginService: LoginService) {}

  ngOnInit (): void {
    this.token = this.loginService.getToken();
  }
}
