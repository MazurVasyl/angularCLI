import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../modules/auth/services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-log-out-button',
  templateUrl: './log-out-button.component.html'
})
export class LogOutButtonComponent implements OnInit {
  token: string;
  href: string;

  constructor (
    private router: Router,
    private loginService: LoginService
  ) {}

  ngOnInit (): void {
    this.token = this.loginService.getToken();
    this.href = this.router.url;
  }

  logOut (): void {
    this.loginService.destroyToken();
  }
}
