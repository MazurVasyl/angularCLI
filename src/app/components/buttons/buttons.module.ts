import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogInButtonComponent } from './log-in-button/log-in-button.component';
import { LogOutButtonComponent } from './log-out-button/log-out-button.component';
import { SignUpButtonComponent } from './sign-up-button/sign-up-button.component';
import { RouterModule } from '@angular/router';
import { MainPageButtonComponent } from './main-page-button/main-page-button.component';
import { OwnPageButtonComponent } from './own-page-button/own-page-button.component';

@NgModule({
  declarations: [
    LogInButtonComponent,
    LogOutButtonComponent,
    SignUpButtonComponent,
    MainPageButtonComponent,
    OwnPageButtonComponent
  ],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [
    LogInButtonComponent,
    LogOutButtonComponent,
    SignUpButtonComponent,
    MainPageButtonComponent,
    OwnPageButtonComponent
  ]
})
export class ButtonsModule { }
