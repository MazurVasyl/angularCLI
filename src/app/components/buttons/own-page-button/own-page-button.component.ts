import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../../rest/http/http.service';
import { LoginService } from '../../../modules/auth/services/login.service';

@Component({
  selector: 'app-own-page-button',
  templateUrl: './own-page-button.component.html'
})
export class OwnPageButtonComponent implements OnInit {
  id: string;
  token: string;

  constructor (
    private http: HttpService,
    private loginService: LoginService
  ) {}

  ngOnInit (): void {
    this.token = this.loginService.getToken();
    this.id = this.loginService.getId();
  }
}
