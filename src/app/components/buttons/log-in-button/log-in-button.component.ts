import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../../modules/auth/services/login.service';

@Component({
  selector: 'app-log-in-button',
  templateUrl: './log-in-button.component.html'
})
export class LogInButtonComponent implements OnInit {
  token: string;

  constructor (private loginService: LoginService) {}

  ngOnInit (): void {
    this.token = this.loginService.getToken();
  }
}
