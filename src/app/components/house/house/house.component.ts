import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import { HousesType } from '../../../types';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-house',
  templateUrl: './house.component.html'
})
export class HouseComponent {

export class HouseComponent implements OnInit {
  id: string;

  @Input() house: HousesType;

  @Output() deleteHouse = new EventEmitter();

  constructor (
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit (): void {
    this.activatedRoute.params.subscribe(params => {
      if (params.id) {
        this.id = params.id;
      }
    });
  }
}
