import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HousesToSaleModule } from './containers/houses-to-sale/houses-to-sale.module';
import { AppRoutingModule } from './app-routing.module';
import { RegistrationModule } from './containers/registration/registration.module';
import { OwnPageModule } from './containers/own-page/own-page.module';
import { AuthModule } from './modules/auth/auth.module';
import { LoginModule } from './containers/login/login.module';
import { LoginService } from './modules/auth/services/login.service';
import { CreateHouseModule } from './containers/create-house/create-house.module';
import { HttpService } from './rest/http/http.service';
import { DetailedInfoModule } from './containers/detailed-info/detailed-info.module';
import { EditHouseModule } from './containers/edit-house/edit-house.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HousesToSaleModule,
    RegistrationModule,
    AppRoutingModule,
    OwnPageModule,
    LoginModule,
    AuthModule,
    CreateHouseModule,
    DetailedInfoModule,
    EditHouseModule
  ],
  providers: [
    HttpService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
