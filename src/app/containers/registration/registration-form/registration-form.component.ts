import { Component, EventEmitter, Input, Output } from '@angular/core';
import { UsersType } from '../../../types';

@Component({
  selector: 'app-registration-form',
  templateUrl: './registration-form.component.html'
})
export class RegistrationFormComponent {
  minName = 3;
  maxName = 9;
  minLogin = 5;
  maxLogin = 15;
  minPassword = 8;
  maxPassword = 12;

  @Input() newUser: UsersType;

  @Output() addNewUser = new EventEmitter<UsersType>();
}
