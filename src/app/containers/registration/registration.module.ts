import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration-routing.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CustomFormsModule } from 'ngx-custom-validators';
import { ParagraphsModule } from '../../components/paragraphs/paragraphs.module';
import { RegistrationFormComponent } from './registration-form/registration-form.component';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    RegistrationComponent,
    RegistrationFormComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    RegistrationRoutingModule,
    FormsModule,
    RouterModule,
    CustomFormsModule,
    ParagraphsModule
  ],
  exports: [
    RegistrationComponent
  ]
})
export class RegistrationModule {}
