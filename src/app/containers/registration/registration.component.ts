import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../rest/http/http.service';

import { UsersType } from '../../types';
import { LoginService } from '../../modules/auth/services/login.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html'
})
export class RegistrationComponent {
  newUser: UsersType = {
    firstName: '',
    lastName: '',
    phone: '',
    email: '',
    login: '',
    password: ''
  };

  constructor (
    private router: Router,
    private loginService: LoginService,
    private http: HttpService
  ) {}

  pushNewUser (): void {
    this.http.pushNewUser(this.newUser).subscribe(res => {
      this.loginService.createToken(res[0].token, res[1]);
      this.router.navigate(['/own-page', `${res[0].userId}`]);
    });
  }
}
