import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditHouseComponent } from './edit-house.component';

const routes: Routes = [
  {
    path: '',
    component: EditHouseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditHouseRoutingModule { }
