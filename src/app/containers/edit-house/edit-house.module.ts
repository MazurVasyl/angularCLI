import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { EditHouseComponent } from './edit-house.component';
import { EditHouseRoutingModule } from './edit-house-routing.module';
import { ButtonsModule } from '../../components/buttons/buttons.module';
import { UserInfoModule } from '../../components/user-info/user-info.module';
import {FormsModule} from '@angular/forms';
import {ParagraphsModule} from '../../components/paragraphs/paragraphs.module';
import {CustomFormsModule} from 'ngx-custom-validators';

@NgModule({
  declarations: [EditHouseComponent],
  imports: [
    CommonModule,
    RouterModule,
    EditHouseRoutingModule,
    ButtonsModule,
    UserInfoModule,
    FormsModule,
    ParagraphsModule,
    CustomFormsModule
  ]
})
export class EditHouseModule { }
