import { Component, OnInit } from '@angular/core';
import { HousesType } from '../../types';
import { HttpService } from '../../rest/http/http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-edit-house',
  templateUrl: './edit-house.component.html',
  styleUrls: ['./edit-house.component.css']
})
export class EditHouseComponent implements OnInit {
  house: HousesType;
  minHouseNumber = 1;
  maxHouseNumber = 500;
  minPrice = 1;
  minRoomCount = 1;

  constructor (
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private http: HttpService
  ) { }

  ngOnInit (): void {
    this.activatedRoute.params.subscribe(params => {
      this.http.getHouseInfo(params.houseId).subscribe(res => {
        this.house = res;
      });
    });
  }

  editHouse (): void {
    this.http.putHouseInfo(this.house).subscribe(res => {
      console.log(res);
      this.router.navigate(['/own-page', `${this.house.userId}`]);
    });
  }
}
