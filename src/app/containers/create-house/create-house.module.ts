import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ngx-custom-validators';
import { HttpClientModule } from '@angular/common/http';

import { CreateHouseComponent } from './create-house.component';
import { CreateHouseRoutingModule } from './create-house.routing.module';
import { ButtonsModule } from '../../components/buttons/buttons.module';
import { ParagraphsModule } from '../../components/paragraphs/paragraphs.module';
import { UserInfoModule } from '../../components/user-info/user-info.module';

@NgModule({
  declarations: [CreateHouseComponent],
  imports: [
    CommonModule,
    CreateHouseRoutingModule,
    ButtonsModule,
    FormsModule,
    CustomFormsModule,
    ParagraphsModule,
    HttpClientModule,
    UserInfoModule
  ],
  exports: [CreateHouseComponent]
})
export class CreateHouseModule { }
