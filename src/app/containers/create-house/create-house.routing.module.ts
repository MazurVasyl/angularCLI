import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateHouseComponent } from './create-house.component';

const routes: Routes = [
  {
    path: '',
    component: CreateHouseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CreateHouseRoutingModule { }
