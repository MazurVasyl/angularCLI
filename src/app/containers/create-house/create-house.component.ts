import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { HouseType } from '../../types';
import { HttpService } from '../../rest/http/http.service';

@Component({
  selector: 'app-create-house',
  templateUrl: './create-house.component.html'
})
export class CreateHouseComponent {
  newHouse: HouseType = {
    city: '',
    street: '',
    houseNumber: 0,
    price: 0,
    roomCount: 0,
    userId: 0
  }

  minHouseNumber = 1;
  maxHouseNumber = 500;
  minPrice = 1;
  minRoomCount = 1;

  constructor (
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private http: HttpService
  ) {}

  ngOnInit (): void {
    this.activatedRoute.params.subscribe(params => {
      this.newHouse.userId = params.id;
    });
  }

  addNewHouse (): void {
    this.http.pushNewHouse(this.newHouse).subscribe(res => {
      this.router.navigate(['/own-page', `${res.userId}`]);
    });
  }
}
