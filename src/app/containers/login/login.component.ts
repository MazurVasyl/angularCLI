import { Component } from '@angular/core';
import { LoginService } from '../../modules/auth/services/login.service';
import { Router } from '@angular/router';

import { HttpService } from '../../rest/http/http.service';
import { DataType } from '../../types';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent {
  error: number;
  data: DataType = {
    login: undefined,
    password: undefined
  };

  constructor (
    private router: Router,
    private loginService: LoginService,
    private http: HttpService
  ) {}

  check () {
    this.http.postLogin(this.data).subscribe(res => {
      this.loginService.createToken(res[0].token, res[1]);
      this.router.navigate(['/own-page', `${res[0].userId}`]);
    },
    error => {
      this.error = error.status;
    });
  }
}
