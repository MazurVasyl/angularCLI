import { Component, OnInit } from '@angular/core';
import {HousesType, UserWithIdType} from '../../types';
import { HttpService } from '../../rest/http/http.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-detailed-info',
  templateUrl: './detailed-info.component.html',
  styleUrls: ['./detailed-info.component.css']
})
export class DetailedInfoComponent implements OnInit {
  house: HousesType;
  user: UserWithIdType;

  constructor (
    private activatedRoute: ActivatedRoute,
    private http: HttpService
  ) {}

  ngOnInit (): void {
    this.activatedRoute.params.subscribe(params => {
      this.http.getHouseInfo(params.houseId).subscribe(house => {
        this.house = house;
        this.http.getUserInfo(this.house.userId).subscribe(user => {
          this.user = user;
        });
      });
    });
  }
}
