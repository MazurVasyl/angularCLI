import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailedInfoComponent } from './detailed-info.component';
import { DetailedInfoRoutingModule } from './detailed-info-routing.module';
import { ButtonsModule } from '../../components/buttons/buttons.module';
import { UserInfoModule } from '../../components/user-info/user-info.module';

@NgModule({
  declarations: [DetailedInfoComponent],
  imports: [
    CommonModule,
    DetailedInfoRoutingModule,
    ButtonsModule,
    UserInfoModule
  ]
})
export class DetailedInfoModule { }
