import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OwnPageComponent } from './own-page.component';
import { CreateHouseComponent } from '../create-house/create-house.component';

const routes: Routes = [
  {
    path: '',
    component: OwnPageComponent
  },
  {
    path: ':id/create',
    component: CreateHouseComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class OwnPageRoutingModule { }
