import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OwnPageComponent } from './own-page.component';
import { OwnPageRoutingModule } from './own-page-routing.module';
import { HouseModule } from '../../components/house/house.module';
import { ButtonsModule } from '../../components/buttons/buttons.module';
import { UserInfoModule } from '../../components/user-info/user-info.module';

@NgModule({
  declarations: [OwnPageComponent],
  imports: [
    CommonModule,
    OwnPageRoutingModule,
    HouseModule,
    ButtonsModule,
    UserInfoModule
  ],
  exports: [OwnPageComponent]
})
export class OwnPageModule {}
