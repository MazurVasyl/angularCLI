import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../rest/http/http.service';
import { HousesType } from '../../types';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-own-page',
  templateUrl: './own-page.component.html'
})
export class OwnPageComponent implements OnInit {
  houses: HousesType[];

  constructor (
    private activatedRoute: ActivatedRoute,
    private http: HttpService,
    private router: Router
  ) {}

  ngOnInit (): void {
    this.activatedRoute.params.subscribe(params => {
      this.http.getUserHouses(params.id).subscribe(res => {
        this.houses = res;
      });
    });
  }

  clickToDeleteHouse (houseId: number): void {
    this.http.deleteHouse(houseId).subscribe(res => {
      this.router.navigate(['/own-page', `${res.userId}`]);
    });
  }
}
