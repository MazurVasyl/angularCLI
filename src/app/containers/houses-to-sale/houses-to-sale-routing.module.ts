import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HousesToSaleComponent } from './houses-to-sale.component';

const routes: Routes = [
  {
    path: '',
    component: HousesToSaleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HousesToSaleRoutingModule { }
