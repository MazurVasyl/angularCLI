import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { HousesToSaleComponent } from './houses-to-sale.component';
import { HouseModule } from '../../components/house/house.module';
import { HousesToSaleRoutingModule } from './houses-to-sale-routing.module';
import { ButtonsModule } from '../../components/buttons/buttons.module';
import { UserInfoModule } from '../../components/user-info/user-info.module';

@NgModule({
  declarations: [HousesToSaleComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    HouseModule,
    RouterModule,
    HousesToSaleRoutingModule,
    ButtonsModule,
    UserInfoModule
  ],
  exports: [
    HousesToSaleComponent
  ]
})
export class HousesToSaleModule {}
