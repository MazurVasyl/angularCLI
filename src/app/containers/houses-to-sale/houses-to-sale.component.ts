import { Component, OnInit } from '@angular/core';
import { HousesType } from '../../types';
import { HttpService } from '../../rest/http/http.service';

@Component({
  selector: 'app-houses-to-sale',
  templateUrl: './houses-to-sale.component.html'
})
export class HousesToSaleComponent implements OnInit {
  houses: HousesType[];

  constructor (
    private http: HttpService
  ) {}

  ngOnInit (): void {
    this.http.getAllHouses().subscribe(res => {
      this.houses = res;
    });
  }

}
