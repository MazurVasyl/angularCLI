module.exports = {
  env: {
    browser: true,
    es2021: true
  },
  extends: [
    'standard'
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module'
  },
  plugins: [
    '@typescript-eslint'
  ],
  rules: {
    semi: ['error', 'always'],
    'no-useless-constructor': 'off',
    'space-before-function-paren': ['error', 'always'],
    'no-self-assign': 'off',
    'no-undef': 'off'
  }
};
