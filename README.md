This will be an application where a registered user can post an advertisement for the sale 
of a house. 
Enter "ng serve" and navigate to "http://localhost:4200/". 
There will be shown the main page. In the corner there are two buttons: to sign up and to log in. 
To log in enter login "pavlo" and password "123" and it will be redirected to the own page of the user. 
To register - fill the form and it will be redirected to the registered user's own page. 
If the user is logged in, there will be shown a button called "log out" instead.

  


# AngularCLI

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.5.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

## Plan
